# Universal converter

`universal-converter` is a program that takes one file tree as an argument, for example your music collection, then applies a conversion command to (not necessarily) all files in the tree and writes a result to some other tree mirroring the structure of the original tree. 

For example: you have a tree containing some mp3 files:
 - dir1/artist1/song1.mp3
 - dir1/artist1/song2.mp3
 - dir1/artist2/album1/song3.mp3
 - dir1/artist2/album1/song4.mp3
 - dir1/artist2/album2/song5.mp3
And, for whatever reason, you want co convert it to ogg vorbis. `universal-converter` can apply your conversion command to each file in a directory (e.g. `ffmpeg` or `oggenc`) and create a mirrored structure in an other directory: 
 - dir2/artist1/song1.ogg
 - dir2/artist1/song2.ogg
 - dir2/artist2/album1/song3.ogg
 - dir2/artist2/album1/song4.ogg
 - dir2/artist2/album2/song5.ogg

Features: 
 - matching files to convert by extension
 - possibility of pass-through (copy files with a particular extension to the output directory without any conversion)
 - incremental conversion: do not convert files for which output file already exists and is newer than source (make-like behavior)
   - additionally: possibility of removing from the output directory files that are not the output of conversion
 - configurable number of conversions done in parallel
 - support for converters that take input/output file as an argument as well as converters that use stdin/stdout for input/output
 
See [the manual](universal-converter.md) for more information.
