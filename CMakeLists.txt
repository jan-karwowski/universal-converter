cmake_minimum_required (VERSION 3.12.0)

set (CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

project(universal-converter)

include(GNUInstallDirs)
option(USE_SANITIZERS "Should address sanitizer and undefined behaviour sanitizer be enabled? (works with clang and gcc)" OFF)

find_program(PANDOC pandoc REQUIRED)

find_package(Boost REQUIRED COMPONENTS program_options log)

add_executable(universal-converter main.cpp)
add_dependencies(universal-converter Boost::program_options)
target_link_libraries(universal-converter PRIVATE stdc++fs Boost::log Boost::program_options)

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang" OR CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  target_compile_options(universal-converter PRIVATE -Wall -Wextra)
else()
  message("Compiler does not support -Wall and -Wextra flags. Not using")
endif()
if(USE_SANITIZERS)
  target_compile_options(universal-converter PRIVATE -fsanitize=address,undefined)
  target_link_libraries(universal-converter PRIVATE -fsanitize=address,undefined -static-libasan)
endif()

add_custom_command(OUTPUT universal-converter.1 MAIN_DEPENDENCY universal-converter.md COMMAND pandoc -f markdown -t man -s -o universal-converter.1  ${CMAKE_CURRENT_SOURCE_DIR}/universal-converter.md)

add_custom_target(universal-converter-man ALL DEPENDS universal-converter.1)


install(FILES ${CMAKE_CURRENT_BINARY_DIR}/universal-converter.1  TYPE MAN)
install(TARGETS universal-converter)
install(DIRECTORY examples DESTINATION ${CMAKE_INSTALL_DOCDIR}/universal-converter)
install(FILES README.md DESTINATION ${CMAKE_INSTALL_DOCDIR}/universal-converter)
