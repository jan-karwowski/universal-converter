#include <algorithm>
#include <any>
#include <cstring>
#include <filesystem>
#include <functional>
#include <iostream>
#include <iterator>
#include <memory>
#include <optional>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <system_error>
#include <utility>

#include <boost/asio.hpp>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>
#include <boost/outcome.hpp>
#include <boost/process.hpp>
#include <boost/program_options.hpp>

namespace fs = std::filesystem;
namespace bp = boost::process;
namespace po = boost::program_options;
namespace asio = boost::asio;
namespace outcome = BOOST_OUTCOME_V2_NAMESPACE;

enum exit_code { SUCCESS = 0, BAD_ARGS = 1 };

struct conversion_settings {
  std::vector<std::string> suffixes_to_process;
  std::string converted_suffix;
  std::vector<std::string> passthru_suffixes;
  bool passthru_everything;
  bool convert_use_stdin;
  bool convert_use_stdout;
  std::vector<std::string>
      conversion_command; // %i replaced with input and %o with output
  bool converter_error_is_fatal;
  int parallel_processes;
  bool remove_nonexistent_files;

  conversion_settings()
      : passthru_everything(false), convert_use_stdin(false),
        convert_use_stdout(false), converter_error_is_fatal(false),
        parallel_processes(1), remove_nonexistent_files(false) {}
};

class process_exited_with_error_category : public std::error_category {
  std::string message(int ec) const override {
    std::ostringstream out;
    out << "Process exited with code " << ec;
    return out.str();
  }

  const char *name() const noexcept override {
    return "Process exited with error";
  }
};

process_exited_with_error_category process_exited_with_error;

class sigint_termination_error_category : public std::error_category {
  const char *name() const noexcept override {
    return "Termination due to sigint";
  }

  std::string message(int) const override {
    return "Termination due to sigint";
  }
};

sigint_termination_error_category sigint_termination;

outcome::result<void, std::error_code>
clear_nonexistent_files(const fs::path &directory_path,
                        const std::set<fs::path> &files_to_keep);

bool is_suffix(const std::string &suffix, const std::string &s) {
  auto ait = suffix.rbegin();
  auto bit = s.rbegin();

  while (ait != suffix.rend() && bit != s.rend() && *ait == *bit) {
    ait++;
    bit++;
  }
  return ait == suffix.rend();
}

class directory_stack_element {
public:
  fs::directory_iterator iterator;
  fs::path relative_path;
  std::set<fs::path> files_found;

  directory_stack_element(fs::directory_iterator &&iterator,
                          const fs::path &relative_path)
      : iterator(iterator), relative_path(relative_path) {}
};

class convert_files_state_machine {
  enum state { NOT_STARTED, RUNNING, INTERRUPT, ALL_STARTED, ALL_FINISHED };

  const conversion_settings &settings;
  asio::io_context context;
  // directory + relative path to it
  std::stack<directory_stack_element> dirs_to_process;
  const fs::path input_dir;
  const fs::path output_dir;
  int running_processes;
  bp::group converter_process_group;
  state conversion_state;
  std::optional<std::error_code> error;
  bool all_started;
  asio::signal_set sigint_set;
  int sigint_counter;

  void handle_error(const std::error_code &ec);
  outcome::result<bool, std::error_code> process_next_file();
  outcome::result<void, std::error_code>
  launch_conversion_process(const fs::path &input, const fs::path &output);
  std::function<void(boost::system::error_code, int)>
  create_termination_handler(fs::path infile, fs::path outfile);
  outcome::result<void, std::error_code>
  ensure_output_directory(const fs::path &relative_path);
  void pump_files_to_process();
  outcome::result<void, std::error_code>
  process_file(const fs::path &relative_path, directory_stack_element& stack_el);
  void critical_error();
  void set_conversion_state(state s);
  void sigint_handler(const std::error_code &ec, int signal);

public:
  outcome::result<bool, std::error_code> run_conversion();
  convert_files_state_machine(const conversion_settings &settings,
                              const fs::path &input_dir,
                              const fs::path &output_dir);
};

static boost::log::trivial::severity_level levels[] = {
    boost::log::trivial::fatal,   boost::log::trivial::error,
    boost::log::trivial::warning, boost::log::trivial::info,
    boost::log::trivial::debug,   boost::log::trivial::trace};

int main(int argc, char **argv) {
  po::options_description desc("transformer options");
  conversion_settings settings;
  std::vector<std::string> main_args;

  std::string input_dir;
  std::string output_dir;
  bool help_switch;
  int verbosity = 2;
  settings.remove_nonexistent_files = false;

  // clang-format off
  desc.add_options()
    ("converted-suffix", po::value(&settings.converted_suffix)->required())
    ("suffix-to-process", po::value(&settings.suffixes_to_process)->required())
    ("passthru-suffix", po::value(&settings.passthru_suffixes))
    ("passthru-everything", po::bool_switch(&settings.passthru_everything),  "Copy all files other than converted-suffixes to outptu directory without any processing")
    ("input-dir", po::value(&input_dir)->required())
    ("output-dir", po::value(&output_dir)->required())
    ("convert-use-stdin", po::bool_switch(&settings.convert_use_stdin))
    ("convert-use-stdout", po::bool_switch(&settings.convert_use_stdout))
    ("conversion-command", po::value(&settings.conversion_command)->required())
    ("converter-error-is-fatal", po::value(&settings.converter_error_is_fatal))
    ("parallel-processes", po::value(&settings.parallel_processes)->default_value(1))
    ("verbose", po::value(&verbosity)->default_value(2))
    ("remove-nonexistent-files", po::bool_switch(&settings.remove_nonexistent_files))
    ("help", po::bool_switch(&help_switch), "program help");
  // clang-format on

  po::positional_options_description positional;
  positional.add("conversion-command", -1);

  po::variables_map vm;
  try {
    po::store(po::command_line_parser(argc, argv)
                  .options(desc)
                  .positional(positional)
                  .run(),
              vm);
    vm.notify();
    verbosity =
        std::clamp<int>(verbosity, 0, sizeof(levels) / sizeof(*levels) - 1);
    boost::log::core::get()->set_filter(boost::log::trivial::severity >=
                                        levels[verbosity]);

  } catch (po::error &err) {
    BOOST_LOG_TRIVIAL(error) << "Error parsing options " << err.what();
    BOOST_LOG_TRIVIAL(info) << desc;
    return BAD_ARGS;
  }

  if (help_switch) {
    BOOST_LOG_TRIVIAL(info) << "Usage:" << desc;
    return SUCCESS;
  } else if (settings.conversion_command.size() < 1) {
    BOOST_LOG_TRIVIAL(error)
        << "Not enough arugments for conversion command (at least one required)"
        << main_args.size();
    return BAD_ARGS;
  } else {
    convert_files_state_machine cfsm(settings, input_dir, output_dir);
    auto res = cfsm.run_conversion();
    if (res) {
      BOOST_LOG_TRIVIAL(info) << "Conversion success";
    } else {
      BOOST_LOG_TRIVIAL(error) << "Error during conversion" << res.error();
    }
  }
}

outcome::result<bool, std::error_code>
input_newer_than_output(const fs::path &in, const fs::path &out) {
  std::error_code ec;
  if (fs::exists(out, ec)) {
    if (ec) {
      return ec;
    }

    auto t1 = fs::last_write_time(in);
    if (ec) {
      return ec;
    }
    auto t2 = fs::last_write_time(out);
    if (ec) {
      return ec;
    }
    return t1 > t2;
  } else {
    return true;
  }
}

void launch_process(const conversion_settings &settings,
                    asio::io_context &context, std::error_code &ec,
                    const fs::path &input_file, const fs::path &output_file,
                    std::vector<std::string> &command,
                    std::function<void(boost::system::error_code, int)> &&func,
                    bp::group &group) {
  if (settings.convert_use_stdin && settings.convert_use_stdout) {
    bp::async_system(context, std::move(func), group,
                     bp::search_path(*settings.conversion_command.begin()),
                     bp::args = command, bp::std_in = input_file,
                     bp::std_out = output_file, ec);
  } else if (settings.convert_use_stdin) {
    bp::async_system(context, std::move(func), group,
                     bp::search_path(*settings.conversion_command.begin()),
                     bp::args = command, bp::std_in = input_file, ec);
  } else if (settings.convert_use_stdout) {
    bp::async_system(context, std::move(func), group,
                     bp::search_path(*settings.conversion_command.begin()),
                     bp::args = command, bp::std_out = output_file, ec);
  } else {
    bp::async_system(context, std::move(func), group,
                     bp::search_path(*settings.conversion_command.begin()),
                     bp::args = command, ec);
  }
  if (ec) {
    BOOST_LOG_TRIVIAL(error) << "Problem when launching process " << ec;
  }
}

static std::vector<std::string>
fill_command_placeholders(const conversion_settings &settings,
                          const fs::path &input_file,
                          const fs::path &output_file) {
  std::vector<std::string> command;
  (void)std::transform(
      ++settings.conversion_command.begin(), settings.conversion_command.end(),
      std::back_insert_iterator(command), [&](auto &str) {
        if (str == "%i" && !settings.convert_use_stdin) {
          return input_file.string();
        } else if (str == "%o" && !settings.convert_use_stdout) {
          return output_file.string();
        } else {
          return str;
        }
      });
  return command;
}

convert_files_state_machine::convert_files_state_machine(
    const conversion_settings &settings, const fs::path &input_dir,
    const fs::path &output_dir)
    : settings(settings), context(), input_dir(input_dir),
      output_dir(output_dir), running_processes(0), converter_process_group(),
      conversion_state(NOT_STARTED), all_started(false),
      sigint_set(context, SIGINT), sigint_counter(0) {}

outcome::result<bool, std::error_code>
convert_files_state_machine::process_next_file() {
  std::optional<std::pair<fs::path, std::reference_wrapper<directory_stack_element>>> file_to_convert;

  while (!file_to_convert.has_value() && !dirs_to_process.empty()) {
    auto &top = dirs_to_process.top();
    if (top.iterator == fs::end(top.iterator)) {
      BOOST_LOG_TRIVIAL(trace) << "Leaving directory " << top.relative_path;
      if (settings.remove_nonexistent_files) {
        BOOST_OUTCOME_TRY(clear_nonexistent_files(
            output_dir / top.relative_path, top.files_found));
      }
      dirs_to_process.pop();
    } else {
      if (top.iterator->is_directory()) {
	top.files_found.insert(top.iterator->path().filename());
        fs::path new_path = top.relative_path / top.iterator->path().filename();
        BOOST_OUTCOME_TRY(ensure_output_directory(new_path));
        dirs_to_process.push(directory_stack_element(
            fs::directory_iterator(input_dir / new_path), new_path));
      } else if (top.iterator->is_regular_file()) {
        file_to_convert = std::make_pair(top.relative_path / top.iterator->path().filename(), std::ref(top));
	// TODO:: 
      } else {
        BOOST_LOG_TRIVIAL(debug)
            << "Skipping a file that is not a directory nor regular file "
            << (top.relative_path / ((*top.iterator).path().filename()));
      }
      top.iterator++;
    }
  }

  if (file_to_convert.has_value()) {
    BOOST_OUTCOME_TRY(process_file(file_to_convert->first, file_to_convert->second));
    return true;
  } else {
    // NOOP, end of dirs to process
    return false;
  }
}

std::string replace_suffix(const std::string &str,
                           const std::string &old_suffix,
                           const std::string &new_suffix) {
  return str.substr(0, str.length() - old_suffix.length()) + new_suffix;
}

outcome::result<void, std::error_code>
convert_files_state_machine::process_file(const fs::path &relative_path, directory_stack_element& element) {
  auto suffix = std::find_if(
      settings.suffixes_to_process.begin(), settings.suffixes_to_process.end(),
      [&](auto &suffix) {
        return is_suffix(suffix, relative_path.filename().string());
      });
  if (suffix != settings.suffixes_to_process.end()) {
    auto input_file = input_dir / relative_path;
    auto output_file = output_dir / replace_suffix(relative_path, *suffix,
                                                   settings.converted_suffix);
    element.files_found.insert(output_file.filename());
    
    BOOST_OUTCOME_TRY(do_run, input_newer_than_output(input_file, output_file));

    if (do_run) {
      BOOST_LOG_TRIVIAL(info) << "Convert file " << input_file.string()
                              << " to " << output_file.string();
      BOOST_OUTCOME_TRY(launch_conversion_process(input_file, output_file));
    } else {
      BOOST_LOG_TRIVIAL(info)
          << "Output newer than input, skipping in:" << input_file.string()
          << " out: " << output_file.string();
    }
  } else if (settings.passthru_everything ||
             std::any_of(settings.passthru_suffixes.begin(),
                         settings.passthru_suffixes.end(),
                         [&relative_path](auto &el) {
                           return is_suffix(el, relative_path);
                         })) {
    std::error_code ec;
    BOOST_LOG_TRIVIAL(info)
        << "Copying input to output without changes " << relative_path;
    element.files_found.insert(relative_path.filename());
    fs::copy_file(input_dir / relative_path, output_dir / relative_path,
                  fs::copy_options::update_existing, ec);
    if (ec) {
      return ec;
    }
  } else {
    BOOST_LOG_TRIVIAL(debug)
        << "File nor marked for conversion of copy, skipping: "
        << relative_path;
  }
  return outcome::success();
}

outcome::result<void, std::error_code>
convert_files_state_machine::launch_conversion_process(const fs::path &input,
                                                       const fs::path &output) {
  auto arguments = fill_command_placeholders(settings, input, output);
  std::error_code ec;
  launch_process(settings, context, ec, input, output, arguments,
                 create_termination_handler(input, output),
                 converter_process_group);
  if (ec) {
    return ec;
  } else {
    running_processes++;
    return outcome::success();
  }
}

outcome::result<void, std::error_code>
convert_files_state_machine::ensure_output_directory(
    const fs::path &relative_path) {
  std::error_code ec;
  fs::create_directories(output_dir / relative_path, ec);
  if (ec) {
    return ec;
  }
  return outcome::success();
}

std::function<void(boost::system::error_code, int)>
convert_files_state_machine::create_termination_handler(const fs::path input,
                                                        const fs::path output) {
  return [this, input, output](boost::system::error_code ec, int exit_status) {
    running_processes--;
    if (ec) {
      BOOST_LOG_TRIVIAL(error)
          << "Problem when launching conversion process for file " << input
          << " " << ec.message();
      error = ec;
      critical_error();
    } else {
      if (exit_status != 0) {
        if (settings.converter_error_is_fatal) {
          BOOST_LOG_TRIVIAL(error) << "Conversion of file " << input
                                   << " failed with code " << exit_status;
          error = std::error_code(exit_status, process_exited_with_error);
          critical_error();
        } else {
          BOOST_LOG_TRIVIAL(warning) << "Conversion of file " << input
                                     << " failed with code " << exit_status;
          boost::asio::post(context,
                            [this]() { this->pump_files_to_process(); });
        }

      } else {
        BOOST_LOG_TRIVIAL(info)
            << "Conversio for file " << input << " finished with sucess";
        boost::asio::post(context, [this]() { this->pump_files_to_process(); });
      }
    }
  };
}

void convert_files_state_machine::critical_error() {
  set_conversion_state(INTERRUPT);
}

void convert_files_state_machine::pump_files_to_process() {
  while (conversion_state == RUNNING &&
         running_processes < settings.parallel_processes) {
    if (auto result = process_next_file()) {
      if (result.value()) {
        // noop
      } else {
        set_conversion_state(ALL_STARTED);
        BOOST_LOG_TRIVIAL(info) << "All conversion processes started";
      }
    } else {
      error = result.error();
      set_conversion_state(INTERRUPT);
    }
  }
  if (running_processes == 0) {
    set_conversion_state(ALL_FINISHED);
    context.stop();
  }
}

void convert_files_state_machine::set_conversion_state(state conversion_state) {
  if (conversion_state > this->conversion_state) {
    this->conversion_state = conversion_state;
    if (conversion_state == ALL_STARTED) {
      all_started = true;
    }
  }
  if (conversion_state == ALL_FINISHED) {
    context.stop(); // Otherwise still waits for signal
  }
}

void convert_files_state_machine::sigint_handler(const std::error_code &ec,
                                                 int) {
  if (ec) {
    error = ec;
    set_conversion_state(ALL_STARTED);
  }
  sigint_counter++;
  sigint_set.async_wait([this](auto a, auto b) { sigint_handler(a, b); });

  switch (sigint_counter) {
  case 1:
    BOOST_LOG_TRIVIAL(warning)
        << "First SIGINT received. Not starting any new conversion processes";
    set_conversion_state(INTERRUPT);
    break;
  case 2:
    BOOST_LOG_TRIVIAL(warning) << "Second SIGINT received. Forcibly "
                                  "terminating all running converters";
    {
      std::error_code ec2;
      converter_process_group.terminate(ec2);
      if (ec2) {
        BOOST_LOG_TRIVIAL(error)
            << "Problem when terminationg convert process group" << ec2;
      }
    }
    break;
  default:
    BOOST_LOG_TRIVIAL(warning)
        << "Process termination already in progress. Ignoring SIGINT";
    break;
  }
}

outcome::result<bool, std::error_code>
convert_files_state_machine::run_conversion() {
  std::error_code ec;

  auto di = fs::directory_iterator(input_dir, ec);
  dirs_to_process.push(directory_stack_element(std::move(di), ""));

  if (ec) {
    return ec;
  }

  sigint_set.async_wait([this](auto a, auto b) { sigint_handler(a, b); });

  set_conversion_state(RUNNING);
  pump_files_to_process();
  BOOST_LOG_TRIVIAL(trace) << "Entering asio run";
  context.run();
  if (error) {
    return error.value();
  } else {
    return all_started;
  }
}

outcome::result<void, std::error_code>
clear_nonexistent_files(const fs::path &directory_path,
                        const std::set<fs::path> &files_to_keep) {
  std::vector<fs::path> files_to_delete;
  {
    std::error_code ec;
    for (auto &el : fs::directory_iterator(directory_path, ec)) {
      if (files_to_keep.find(el.path().filename()) == files_to_keep.end()) {
        files_to_delete.push_back(el.path());
      }
    }
    if (ec) {
      return ec;
    }
  }
  for (auto &path : files_to_delete) {
    std::error_code ec;
    BOOST_LOG_TRIVIAL(info)
        << "Removing file that does not exist in source dir " << path;
    fs::remove_all(path, ec);
    if (ec) {
      return ec;
    }
  }

  return outcome::success();
}
