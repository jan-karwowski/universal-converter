#!/bin/sh

# The script can be used to convert your music collection from flac and ogg formats to mp3
# It converts all flac and ogg files with ffmpeg and copies all mp3 files without recoding

if [ $# -ne 2 ] ; then
    echo exactly two arguments input dir and output dir
    exit 2
fi

./universal-converter --remove-nonexistent-files --parallel-processes 4 --converted-suffix .mp3 --suffix-to-process .flac --suffix-to-process .ogg --passthru-suffix .mp3 --input-dir "$1" --output-dir "$2" --verbose 4 -- ffmpeg  -nostdin -i %i -c:a libmp3lame -b:a 192k -abr:a 1 -y -map_metadata 0 -map_metadata 0:s:0 -id3v2_version 3 -vn %o
    
