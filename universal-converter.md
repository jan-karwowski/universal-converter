% universal-converter(1) | apply conversion command for each file in directory

# Name
universal-converter -- run conversion command for each file in a directory and write output to another directory preserving the directory structure

# Synopsis
`universal-converter --converted-suffix <conv-suffix> --input-dir <input-dir> --output-dir <output-dir> [options] [--] <conversion-command>`

# Description

Apply conversion command for each matching file in `input-dir` and put an output file in `output-dir` file name (w.r.t. suffix, described later) and a relative path within `input_dir` is preserved withing `output_dir`. The matching filed are defined by `--suffix-to-process` options: all files with matching suffix are processed. The files matching `--passthru-suffix` are simply copied without any conversion (still preserving file path and name). Files that do not match `--suffix-to-process` or `--passthru-suffix` are skipped. For copied (passthru) The relative path of the converted file under the `output-dir` is the same as the path of the input file under the `input-dir`. In case of converted files (`suffix-to-process`) the path is also preseved but the suffix of the file name (`suffix-to-process`) is replaced with `conv-suffix`. If the respective file in the output directory exists then mtimes are compared. If the input is newer than output the input is overwritten. Otherwise no action is executed.

## Conversion command

The conversion command should be given as a trailing arguments. The first of the arguments is the command to be run followed by any number of arguments for the command. `%i` argument will be replaced with a path to the input file and %o will be replaced with a path to the output. The command is always run with the working directory where `universal-converter` was started.

## Options

`--converted-suffix` _CONV-SUFFIX_ 

: a suffix that is used as a replacement of argumetns of `--suffix-to-process` option

`--suffix-to-process` _INPUT-SUFFIX_

: a suffix of files that will be processed with the conversion command. Each file which name ends with _INPUT-SUFFIX_ will be passed as an input to the conversion command. This option can be given multiple times and all the suffixes given will be processed.

`--passthru-suffix` _PASSTHRU_SUFFIX_

: a suffix of files that will be copied to the output directory as-is.

`--passthru-everything`

: all files that do not match converted-suffix will be copied to the output directory.


`--input-dir` _INPUT-DIR_

: a directory where files to be converted are located

`-output-dir` _OUTPUT-DIR_

: a directory where output files will be put. The directory must exist.

`--converter-error-is-fatal`

: if this switch is present any conversion command failures (exit status different than 0) will be treaten as a fatal error and the conversion process will be terminated. Otherwise they will be logged as warnings.

`--parallel-processes` _PROCESS_NUM_

: a positive integer defining how many conversion commands can be run in parallel. The default is 1 (sequential operation).

`--remove-nonexistent-files`

: remove files in the output directory that are not result of transformation of copying

`--verbose` _LEVEL_

: set the output verbosity level. Level is an integer from 0 to 5 (inclusive) where 0 reports critical failures only and 5 is very verbose

## Signals

Upon SIGINT delivery the program stops processing any further files and waits for running conversions to complete. To interrupt running conversions the user needs to send SIGINT twice.

# Exmaples

To convert a music collection in the flac format to mp3 using ffmpeg and convert 4 files in parallel. If any of files is already in mp3 format just copy it to the output directory

```
universal-converter --parallel-processes 4 --converted-suffix .mp3 --suffix-to-process .flac --passthru-suffix .mp3 --input-dir my_flac_music_dir --output-dir /media/my-mp3-player/ -- ffmpeg  -i %i -c:a libmp3lame -b:a 192k -abr:a 1 -y -map_metadata 0 -map_metadata 0:s:0 -id3v2_version 3 -vn %o
```
